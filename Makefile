# Directories
OUT_DIR = bin
REPORT_DIR = report

# Files
REPORT = main

.PHONY: clean build write-report

build: $(OUT_DIR)/$(REPORT).pdf

clean:
	@rm -rf $(OUT_DIR)/*

write-report: $(REPORT_DIR)/$(REPORT).tex
	latexmk -pdf -pvc -cd $< -aux-directory=$(OUT_DIR) -output-directory=$(OUT_DIR)

$(OUT_DIR)/$(REPORT).pdf: $(REPORT_DIR)/$(REPORT).tex
	# If an error occurs, consider running apt-get install texlive-font-utils
	latexmk -pdf -cd $< -aux-directory=$(OUT_DIR) -output-directory=$(OUT_DIR)
